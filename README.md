Entity log is used for tracking logs of field values changes on entities
You're able to select which fields you want to track on which entity and
bundle and whether this will be logged in logger or in entity.

Go to admin/config/entity-log and select entities on which you're going to
track changes, select fields for logging and save configuration
