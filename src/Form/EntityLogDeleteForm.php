<?php

namespace Drupal\entity_log\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Entity log entities.
 *
 * @ingroup entity_log
 */
class EntityLogDeleteForm extends ContentEntityDeleteForm {


}
